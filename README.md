Task 1.3 for SWENG CSU33012 - Assignment 1 -Microservices and Spring Boot

GET @ "/messages" to view all messages.

POST @ "/createMessage" to create message,
with body:
{
    "messageKey: "message"
}
