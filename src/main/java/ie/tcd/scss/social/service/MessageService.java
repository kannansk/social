package ie.tcd.scss.social.service;

import ie.tcd.scss.social.domain.Message;
import ie.tcd.scss.social.repo.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public void createMessage(String str) {
        String key = str;
        String title = generateTitle(str);
        String body = generateBody(str);

        Message existingMessage = messageRepository.findByMessageKey(key);
        if (existingMessage == null) {
            Message message = new Message(key, title, body);
            messageRepository.save(message);
        }
    }

    private String generateTitle(String str) {
        return new String(new char[5]).replace('\0', str.charAt(0));
    }

    private String generateBody(String str) {
        return new String(new char[10]).replace('\0', str.charAt(0));
    }
}