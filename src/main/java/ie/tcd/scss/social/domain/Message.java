package ie.tcd.scss.social.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Message {

    @Id
    private String messageKey;
    private String title;
    private String body;

    // Constructors, getters, and setters

    public Message() {
        // Default constructor for JPA
    }

    public Message(String messageKey, String title, String body) {
        this.messageKey = messageKey;
        this.title = title;
        this.body = body;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}