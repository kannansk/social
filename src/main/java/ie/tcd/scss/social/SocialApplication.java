package ie.tcd.scss.social;

import ie.tcd.scss.social.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SocialApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocialApplication.class, args);
    }

    @Bean
    public CommandLineRunner initData(MessageService messageService) {
        return args -> {
            // Initialize the database
            messageService.createMessage("a");
            messageService.createMessage("ab");
            messageService.createMessage("xyz");
        };
    }
}
