package ie.tcd.scss.social.repo;

import ie.tcd.scss.social.domain.Message;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, String> {
    Message findByMessageKey(String messageKey);
}

