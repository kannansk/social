package ie.tcd.scss.social.repo;

import ie.tcd.scss.social.domain.Message;
import ie.tcd.scss.social.service.MessageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the MessageRepository class. Accesses the repository directly, i.e. through method calls and without using
 * the REST API.
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public class MessageRepositoryMockingTest {

    @Autowired
    private MessageRepository messageRepository;

    @MockBean
    private MessageService messageService; // not used

    @Test
    public void testCreateAndFindMessage() {
        Message message = new Message("key1", "Test Title", "Test Body");
        messageRepository.save(message);
        Optional<Message> foundMessage = messageRepository.findById("key1");
        assertTrue(foundMessage.isPresent());
        assertEquals("key1", foundMessage.get().getMessageKey());
        assertEquals("Test Title", foundMessage.get().getTitle());
        assertEquals("Test Body", foundMessage.get().getBody());
    }

    @Test
    public void testUpdateMessage() {
        Message message = new Message("key2", "Test Title", "Test Body");
        messageRepository.save(message);
        message.setTitle("Updated Title");
        messageRepository.save(message);
        Optional<Message> foundMessage = messageRepository.findById("key2");
        assertTrue(foundMessage.isPresent());
        assertEquals("Updated Title", foundMessage.get().getTitle());
    }

    @Test
    public void testDeleteMessage() {
        Message message = new Message("key3", "Test Title", "Test Body");
        messageRepository.save(message);
        messageRepository.deleteById("key3");
        Optional<Message> foundMessage = messageRepository.findById("key3");
        assertTrue(foundMessage.isEmpty());
    }

    @Test
    public void testFindAllMessages() {
        // In this kind of test, the application is NOT started, so the database should be empty.
        Iterable<Message> messagesIterable1 = messageRepository.findAll();
        List<Message> allMessages1 = StreamSupport.stream(messagesIterable1.spliterator(), false)
                .toList();

        Message message1 = new Message("key4", "Test Title 1", "Test Body 1");
        Message message2 = new Message("key5", "Test Title 2", "Test Body 2");
        messageRepository.save(message1);
        messageRepository.save(message2);


        Iterable<Message> messagesIterable2 = messageRepository.findAll();
        List<Message> allMessages2 = StreamSupport.stream(messagesIterable2.spliterator(), false)
                .toList();

        assertEquals(2, allMessages2.size());
        assertEquals("key4", allMessages2.get(0).getMessageKey());
        assertEquals("Test Title 1", allMessages2.get(0).getTitle());
        assertEquals("Test Body 1", allMessages2.get(0).getBody());
        assertEquals("key5", allMessages2.get(1).getMessageKey());
        assertEquals("Test Title 2", allMessages2.get(1).getTitle());
        assertEquals("Test Body 2", allMessages2.get(1).getBody());
    }

    @Test
    public void testFindNonExistingMessage() {
        Optional<Message> foundMessage = messageRepository.findById("nonexistentKey");
        assertTrue(foundMessage.isEmpty());
    }
}
