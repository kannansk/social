package ie.tcd.scss.social.repo;

import ie.tcd.scss.social.domain.Message;
import ie.tcd.scss.social.domain.MessageResponseDTO;
import ie.tcd.scss.social.domain.MessagesResponseDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for the MessageRepository class. Accesses the repository through the provided REST API, i.e. HTTP requests.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class MessageRepositoryRestApiTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private String getRootUrl() {
        return "http://localhost:" + port + "/messages";
    }

    @Test
    public void testCreateAndFindMessage() {
        Message message = new Message("key1unique", "Test Title", "Test Body");

        // Creating the message
        ResponseEntity<MessageResponseDTO> response1 = restTemplate.postForEntity(getRootUrl(), message, MessageResponseDTO.class);
        assertThat(response1.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response1.getBody()).isNotNull();
        assertThat(response1.getBody().getMessageKey()).isEqualTo("key1unique");
        assertThat(response1.getBody().getTitle()).isEqualTo("Test Title");
        assertThat(response1.getBody().getBody()).isEqualTo("Test Body");

        // Finding the message by key
        String entityUrl = getRootUrl() + "/key1unique";
        ResponseEntity<MessageResponseDTO> response2 = restTemplate.getForEntity(entityUrl, MessageResponseDTO.class);
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response2.getBody()).isNotNull();
        assertThat(response2.getBody().getMessageKey()).isEqualTo("key1unique");
        assertThat(response2.getBody().getTitle()).isEqualTo("Test Title");
        assertThat(response2.getBody().getBody()).isEqualTo("Test Body");
    }

    @Test
    public void testFindAllMessages() {
        // Given: We assume that at the start of the application three messages (a, ab, xyz) were created.
        assertMessagesCount(3);
        printMessagesDetails();

        // When: A new message is added
        Message message = new Message("key1unique", "Test Title", "Test Body");
        ResponseEntity<MessageResponseDTO> response2 = restTemplate.postForEntity(getRootUrl(), message, MessageResponseDTO.class);

        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        MessageResponseDTO responseBody = response2.getBody();
        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getMessageKey()).isEqualTo("key1unique");
        assertThat(responseBody.getTitle()).isEqualTo("Test Title");
        assertThat(responseBody.getBody()).isEqualTo("Test Body");

        // Then: A total of four messages should be present now
        assertMessagesCount(4);
        printMessagesDetails();
    }

    /**
     * Helper method to assert the number of messages in the repository.
     * @param expectedCount
     */
    private void assertMessagesCount(int expectedCount) {
        ResponseEntity<MessagesResponseDTO> response = restTemplate.getForEntity(getRootUrl(), MessagesResponseDTO.class);

        assertThat(response.getStatusCode())
                .as("Check if the status code is OK")
                .isEqualTo(HttpStatus.OK);

        assertThat(response.getBody())
                .as("Check if the body is not null")
                .isNotNull();

        List<Message> messages = response.getBody().getEmbeddedMessages().getMessages();
        assertThat(messages.size())
                .as("Check if the number of messages is %d", expectedCount)
                .isEqualTo(expectedCount);
    }

    /**
     * Helper method to print the details of all messages in the repository.
     */
    private void printMessagesDetails() {
        ResponseEntity<MessagesResponseDTO> response = restTemplate.getForEntity(getRootUrl(), MessagesResponseDTO.class);

        if(response.getStatusCode() == HttpStatus.OK && response.getBody() != null) {
            List<Message> messages = response.getBody().getEmbeddedMessages().getMessages();

            if (messages.isEmpty()) {
                System.out.println("No messages in the repository.");
            } else {
                System.out.println("Messages in the repository:");
                for (Message message : messages) {
                    System.out.println("   MessageKey: " + message.getMessageKey() + ", Title: " + message.getTitle() + ", Body: " + message.getBody());
                }
            }
        } else {
            System.out.println("Failed to retrieve messages. Status code: " + response.getStatusCode());
        }
    }

    // more tests
}
